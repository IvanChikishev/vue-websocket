import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Socket from 'socket.io-client';

Vue.config.productionTip = true

const routes = [
    {
        path: '/',
        component: () => import('./pages/home')
    }
];

const router = new VueRouter({
    routes,
    mode: 'history'
});

//http://hub.stage.cratko.ru/
const socket = new Socket('https://hub.stage.cratko.ru', {
    query: {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkZXZpY2VJZCI6ImMzODVmNzY5LTc1NjMtNGRhOC04ZjBmLTQ0NGI0NDI5NjYxZiIsImlhdCI6MTU5NjEzNDczMiwiZXhwIjoxNjI3NjcwNzMyfQ.v9tLVGALLgAKG0OEIegmxA0UaqQmLAVeCok_9UM6_tM'
    },
});

const ROUTES_PATH = {
    'home': '/',
    'apps': '/apps'
}

socket.on('connect', () => {
    console.log('socket connected');

    socket.emit('sign', {
        client: socket.id
    });

    socket.on('session/sign/success', (info) => {
        console.log('session successful initialized')

        // разрешаем рендеринг Vue приложения (не совсем верное решение, сделано для примера)
        Vue.use(VueRouter)
        Vue.use({
            install(Vue) {
                Vue.prototype.$socket = function () {
                    return socket;
                };

                Vue.prototype.socketInfo = info;
            }
        });

        new Vue({
            router,
            render: h => h(App),
        }).$mount('#app')
    });
    socket.on('system/device/status', () => {
        socket.emit('system/device/status', {
            orientation: 1,
            battery: Math.ceil(Math.random() * 100)
        })
    });

    socket.on('navigation/route', async (state) => {
        console.log('route called')
        if(state.success) {
            console.log('successful redirect');

            await router.push(ROUTES_PATH[state.link]);
        }
    });


    socket.on('disconnect', () => {
        console.warn('socket disconnected');
    })
});

